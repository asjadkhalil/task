const path = require('path');
const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');
const request = require('request');
const cheerio = require('cheerio');
const urlExists = require('url-exists');
let rp = require('request-promise');
const app = express();

app.set('view engine', 'ejs');
app.set('views', 'views');


app.get('/I/want/title', async (req, res, next) => {
    console.log("Package doped");

    let address = req.query.address;

    let $;
    let title;
    let returnArray = [];
    let completedRequests = 0;
    //     for(let i = 0; i < address.length; i++) {
    //     await urlExists(address[i] , function(err, exists) {
    //      console.log(exists); // true
    //      if(!exists)
    //         return;
    //     });
    // };
    if (Array.isArray(address)) {
        await address.forEach(async element => {
            try {

                const url = await rp(element);
                $ = cheerio.load(url);
                title = $("title").text();
                console.log('with callback', title);
                returnArray.push(title);
                completedRequests++;
                if (completedRequests == address.length) {
                    res.render('output', {
                        titles: returnArray
                    })
                };

            } catch (e) {
                console.log(e.message);
                let arr = [];
                arr.push(e.message);
                res.render('error', {
                    error: arr
                });

            };
        });
    }


    if (!Array.isArray(address)) {
        let newAddress = [];
        newAddress.push(address);
        await newAddress.forEach(async element => {
            try {

                const url = await rp(element);
                $ = cheerio.load(url);
                title = $("title").text();
                console.log('with callback', title);
                returnArray.push(title);
                completedRequests++;
                if (completedRequests == newAddress.length) {
                    res.render('output', {
                        titles: returnArray
                    })
                };

            } catch (e) {
                console.log(e);
                //console.log(e.cause.host);
                let arr = [];
                arr.push(e.message);
                res.render('error', {
                    error: arr
                });

            };
        });
    }
});


app.get('*', function (req, res) {
    res.status(404).send('Lannisters send their regards!');
});

app.listen(3000);
console.log('Server Running');