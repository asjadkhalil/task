const path = require('path');
const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');
const request = require('request');
const cheerio = require('cheerio');

let rp = require('request-promise');
const app = express();

app.set('view engine', 'ejs');
app.set('views', 'views');


app.get('/I/want/title', (req, res) => {
    console.log("We're in the zone");

    let address = req.query.address;

    console.log('add1', address);
    console.log('add1', address.length);

    let $;
    let title;
    let returnArray = [];
    let completedRequests = 0;

    // const promises = address.map(url => rp(url));
    // Promise.all(promises).then((data)=> {
    //     for (let i = 0; i < address.length; i++ ) { 
    //     $ = cheerio.load(data[i]);
    //      title = $("title").text();
    //      console.log('with callback', title);
    //      returnArray.push(title);
    //      completedRequests++;
    //     }
    //     return returnArray
    // })
    // .then( titles => {
    //     res.render('output', {
    //        titles: titles
    //     });
    // })
    // .catch(err => console.log(err));
    if (Array.isArray(address)) {
        for (let i = 0; i < address.length; i++) {
            rp(address[i]).then(result => {
                    $ = cheerio.load(result);
                    title = $("title").text();
                    console.log('with callback', title);
                    returnArray.push(title);
                    completedRequests++;

                    if (completedRequests == address.length) {
                        res.render('output', {
                            titles: returnArray
                        })
                    }

                })
                .catch(err => {
                    console.log(err.message);
                    let arr = [];
                    arr.push(err.message);
                    res.render('error', {
                        error: arr
                    });
                });
        }
    };


    if (!Array.isArray(address)) {
        let newAddress = [];
        newAddress.push(address);
        rp(newAddress[0]).then(result => {
                $ = cheerio.load(result);
                title = $("title").text();
                console.log('with callback', title);
                returnArray.push(title);
                completedRequests++;

                if (completedRequests == newAddress.length) {
                    res.render('output', {
                        titles: returnArray
                    })
                }

            })
            .catch(err => {
                console.log(err);
                let arr = [];
                arr.push(err.message);
                res.render('error', {
                    error: arr
                });
            });
    }

});

app.get('*', function (req, res) {
    res.status(404).send('Lannisters send their regards!');
});

app.listen(3000);
console.log('Server Running');
