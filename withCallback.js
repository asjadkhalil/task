const path = require('path');
const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');
const request = require('request');
const cheerio = require('cheerio');
const urlExists = require('url-exists');
let rp = require('request-promise');
const app = express();

app.set('view engine', 'ejs');
app.set('views', 'views');


app.get('/I/want/title', async (req, res) => {
    console.log("We're ready to deliver the payload, Alpha");

    let address = req.query.address;

    console.log('add1', address);
    console.log('add1', address.length);

    let $;
    let title;
    let returnArray = [];
    let completedRequests = 0;

    if (Array.isArray(address)) {
        for (let i = 0; i < address.length; i++) {
            request(address[i], function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    $ = cheerio.load(body);
                    title = $("title").text();
                    console.log('with callback', title);
                    returnArray.push(title);
                    completedRequests++;

                    if (completedRequests == address.length) {
                        res.render('output', {
                            titles: returnArray
                        })
                    }
                }

                if (error) {
                    console.log(error);
                    let arr = [];
                    arr.push(error.host);
                    res.render('error', {
                        error: arr
                    });
                }
            });
        };
    }

    if (!Array.isArray(address)) {
        let arra = [];
        arra.push(address);
        console.log('ARRA', arra);
        request(';alskkd', function (error, response, body) {
            request(arra[0], function (error, response, body) {
                console.log(error);
                if (!error && response.statusCode == 200) {
                    $ = cheerio.load(body);
                    title = $("title").text();
                    console.log('with callback', title);
                    returnArray.push(title);
                    completedRequests++;

                    if (completedRequests == arra.length) {
                        res.render('output', {
                            titles: returnArray
                        })
                    }
                }
                if (error) {
                    console.log(error);
                    let arr = [];
                    arr.push(error.host);
                    res.render('error', {
                        error: arr
                    });
                }
            });

        });
    }
});

app.get('*', function (req, res) {
    res.status(404).send('Lannisters send their regards!');
});

app.listen(3000);
console.log('Server Running');